package com.example.sibtainraza.hssmscaster;

import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import com.example.sibtainraza.hssmscaster.utils.AppLog;

public class HSSMSCaster extends Application {

    private static final String TAG = HSSMSCaster.class.getSimpleName();

    private static HSSMSCaster instance;

    public static HSSMSCaster getInstance(){ return instance; }


    @Override
    public void onCreate() {
        super.onCreate();
        AppLog.i(TAG,"onCreate");
        instance = this;
    }

    public static boolean checkPermissionWriteExternal(){
        int writePerm;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            try {
                writePerm = ContextCompat.checkSelfPermission(HSSMSCaster.getInstance(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            } catch (Exception e){
                e.printStackTrace();
                return false;
            }
            return writePerm == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }

}
