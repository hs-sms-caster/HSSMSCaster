package com.example.sibtainraza.hssmscaster.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.model.Group;

import java.util.ArrayList;

/**
 * Created by Sibtain Raza on 11/5/2016.
 */
public class HomeFragmentCardAdapter extends RecyclerView.Adapter<HomeFragmentCardAdapter.HomeViewHolder> {
    private Context mContext;

    ArrayList<Group> listViewGroup = new ArrayList<Group>();

    @Nullable
    private ItemSelectedListener itemSelectedListener;

    public HomeFragmentCardAdapter(ArrayList<Group> listViewGroup){
        this.listViewGroup = listViewGroup;
    }

    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_layout,parent,false);
        HomeViewHolder homeViewHolder = new HomeViewHolder(view);
        return homeViewHolder;
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, int position) {
        final Group group = listViewGroup.get(position);

        holder.tVGroupNo.setText("Group No. "+group.getGroupID());
        holder.tVGroupName.setText(group.getGroupName());
        holder.tVGroupDescription.setText(group.getGroupDescription());

        holder.groupCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemSelectedListener != null){
                    itemSelectedListener.onItemSelected(group);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listViewGroup.size();
    }


    public void setItemSelectedListener(@Nullable ItemSelectedListener itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder{

        TextView tVGroupNo;
        TextView tVGroupName;
        TextView tVGroupDescription;
        LinearLayout groupCardView;

        public HomeViewHolder(View itemView) {
            super(itemView);
            tVGroupNo = (TextView) itemView.findViewById(R.id.tVCardGroupNo);
            tVGroupName = (TextView) itemView.findViewById(R.id.tVCardViewGroupName);
            tVGroupDescription = (TextView) itemView.findViewById(R.id.tVCardViewGroupDescription);
            groupCardView = (LinearLayout) itemView.findViewById(R.id.cardView);
        }
    }

    public interface ItemSelectedListener{
        public void onItemSelected(Group group);
    }
}
