package com.example.sibtainraza.hssmscaster.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.ui.fragments.GroupTabLayoutFragment1;
import com.example.sibtainraza.hssmscaster.ui.fragments.GroupTabLayoutFragment2;
import com.example.sibtainraza.hssmscaster.ui.fragments.GroupTabLayoutFragment3;

/**
 * Created by panacloud on 6/27/15.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Group groupObj;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, Group groupObj) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.groupObj = groupObj;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                GroupTabLayoutFragment1 tab1 = GroupTabLayoutFragment1.newInstance(groupObj);
                return tab1;
            case 1:
                GroupTabLayoutFragment2 tab2 = GroupTabLayoutFragment2.newInstance(groupObj);
                return tab2;
            case 2:
                GroupTabLayoutFragment3 tab3 = GroupTabLayoutFragment3.newInstance(groupObj);
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
