package com.example.sibtainraza.hssmscaster.adapters;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.model.User;
import com.example.sibtainraza.hssmscaster.model.UserContacts;
import com.example.sibtainraza.hssmscaster.model.response.UserContactsResp;
import com.example.sibtainraza.hssmscaster.network.NetworkConstants;
import com.example.sibtainraza.hssmscaster.network.NetworkHandler;
import com.example.sibtainraza.hssmscaster.ui.fragments.GroupTabLayoutFragment1;
import com.example.sibtainraza.hssmscaster.utils.SharedPreferenceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserAddDeleteMemberAdapter extends BaseAdapter {

    ArrayList<User> listViewUser;
    Context context;
    Map<User, Boolean> map = new HashMap<User, Boolean>();
    public static final int GROUP_MEMBER_DELETE = 1, GROUP_MEMBER_ADD = 2;
    private int switchValue;
    ArrayList<User> userList = new ArrayList<User>();
    private Group groupObj;

    ClickEventCallback clickEventCallback;//call this member when button is pressed

    public UserAddDeleteMemberAdapter(Context context, int switchValue, ArrayList<User> listViewUser, Group group) {
        this.context = context;
        this.switchValue = switchValue;
        this.listViewUser = listViewUser;
        this.groupObj = group;
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            switch (switchValue) {
                case UserAddDeleteMemberAdapter.GROUP_MEMBER_DELETE:
                    view = inflater.inflate(R.layout.single_item_lv_delete_members, viewGroup, false);
                    break;
                case UserAddDeleteMemberAdapter.GROUP_MEMBER_ADD:
                    view = inflater.inflate(R.layout.single_item_lv_add_members, viewGroup, false);
                    break;

                default:
                    Log.d("popupMenu", " No popupMenu called");
            }
        }

        final TextView userName = (TextView) view.findViewById(R.id.tVUserName);
        final TextView userContact = (TextView) view.findViewById(R.id.tvUserContact);
        Button btnAddMember = (Button) view.findViewById(R.id.btnAddOrDelete);

        final User user = listViewUser.get(i);

        userName.setText(user.getUserName());
        userContact.setText(user.getUserContact());

        btnAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (switchValue) {
                    case UserAddDeleteMemberAdapter.GROUP_MEMBER_DELETE:

                        UserContacts deleteUserContact = new UserContacts(SharedPreferenceManager.getManager().getUserId(),
                                groupObj.getGroupID(),user.getUserName(),user.getUserContact());
                        Call<UserContactsResp> deleteUserContactsRespCall = NetworkHandler.getInstance().getServices().deleteUserContact(deleteUserContact);

                        deleteUserContactsRespCall.enqueue(new Callback<UserContactsResp>() {
                            @Override
                            public void onResponse(Call<UserContactsResp> call, Response<UserContactsResp> response) {
                            }

                            @Override
                            public void onFailure(Call<UserContactsResp> call, Throwable t) {
                            }
                        });

                        break;
                    case UserAddDeleteMemberAdapter.GROUP_MEMBER_ADD:
                            clickEventCallback.onItemSelected((User)getItem(i));
//                        removeItem((User) getItem(i));
                        break;

                    default:
                        Log.d("popupMenu", " No popupMenu called");
                }


//                Toast.makeText(context,"Clicked Username : " + listViewUser.get(i).getUserName(),Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    @Override
    public int getCount() {
        return listViewUser.size();
    }

    @Override
    public Object getItem(int i) {
        return listViewUser.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void removeItem(User user) {
        listViewUser.remove(user);
        notifyDataSetChanged();
    }


    public ArrayList<User> getSelectedUser() {
        return userList;
    }

    public interface ClickEventCallback{
        void onItemSelected(User user);
    }

    public void setClickEventCallback(ClickEventCallback clickEventCallback){
        this.clickEventCallback = clickEventCallback;
    }
}