package com.example.sibtainraza.hssmscaster.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.model.User;
import com.example.sibtainraza.hssmscaster.model.UserContacts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserMemberSelectionAdapter extends BaseAdapter {

    ArrayList<User> listViewUser;
    Context context;
    Map<User, Boolean> map = new HashMap<User,Boolean>();

    public UserMemberSelectionAdapter(Context context, ArrayList<User> listViewUser) {
        this.context = context;
        this.listViewUser = listViewUser;
        this.map = map;
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.single_item_lv_select_members, viewGroup, false);
            }

        TextView userName = (TextView) view.findViewById(R.id.tVUserName);
        TextView userContact = (TextView) view.findViewById(R.id.tvUserContact);
        CheckBox cBSelectMember  = (CheckBox) view.findViewById(R.id.checkboxSelectMember);

        final User user = listViewUser.get(i);

        userName.setText(user.getUserName());
        userContact.setText(user.getUserContact());
        cBSelectMember.setChecked(user !=null && user.isChecked());
        cBSelectMember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                user.setChecked(b);
            }

        });

        return view;
    }

    @Override
    public int getCount() {
        return listViewUser.size();
    }

    @Override
    public Object getItem(int i) {
        return listViewUser.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public ArrayList<User> getSelectedUser(){
        ArrayList<User> userList = new ArrayList<User>();

        for (User user :
                listViewUser) {
            if (user.isChecked()){
                userList.add(user);
            }
        }

        return userList;
    }

    public void selectAll(boolean b){
        for (User user :
                listViewUser) {
            user.setChecked(b);
        }
        notifyDataSetChanged();
    }

}