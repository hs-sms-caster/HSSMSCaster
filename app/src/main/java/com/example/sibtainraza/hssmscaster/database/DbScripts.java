package com.example.sibtainraza.hssmscaster.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.sibtainraza.hssmscaster.HSSMSCaster;
import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.model.User;
import com.example.sibtainraza.hssmscaster.model.UserContacts;
import com.example.sibtainraza.hssmscaster.utils.AppLog;

import java.util.ArrayList;

/**
 * Created by Muhammad Faizan Khan on 7/19/2016.
 */

public class DbScripts {
    private static final String TAG = DbScripts.class.getSimpleName();
    private static SQLiteDatabase database;

    static {
        if (HSSMSCaster.checkPermissionWriteExternal()) {
            getDatabase();
        }
    }


    private static void getDatabase() {
        DbHandler dbHandler = DbHandler.getInstance();
        dbHandler.openDataBase();
        database = dbHandler.getDatabase();
        AppLog.i(TAG, "getDatabase --- database = " + database);
    }

    /*
  ------------------------------------------------------------------------------------
  Operations For UserContacts
  ------------------------------------------------------------------------------------
*/
    //----- insertAllUserContacts
    public static void insertAllUserContacts(ArrayList<UserContacts> userContactsArrayList){

        for (int i = 0; i < userContactsArrayList.size(); i++) {

            ContentValues values = new ContentValues();

            values.put(FeedReaderContract.UserContacts.PERSON_ID, userContactsArrayList.get(i).getPersonID());
            values.put(FeedReaderContract.UserContacts.USER_ID, userContactsArrayList.get(i).getUserID());
            values.put(FeedReaderContract.UserContacts.GROUP_ID, userContactsArrayList.get(i).getGroupID());
            values.put(FeedReaderContract.UserContacts.PERSON_CONTACT_NAME, userContactsArrayList.get(i).getPersonContactName());
            values.put(FeedReaderContract.UserContacts.PERSON_CONTACT_NO, userContactsArrayList.get(i).getPersonContactNo());

            if (database == null || !database.isOpen()) {
                getDatabase();
            }
            synchronized (database) {
                database.insert(FeedReaderContract.UserContacts.TABLE_NAME, null, values);
            }
        }
    }

    //----- getAllUserContacts
    public static ArrayList<UserContacts> getAllUserContacts() {
        Log.i(TAG, "getAllUserContacts: ");
        ArrayList<UserContacts> userContactsList = new ArrayList<>();
        String query = "SELECT * FROM " + FeedReaderContract.UserContacts.TABLE_NAME;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        UserContacts userContacts = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                userContacts = new UserContacts();
//
                userContacts.setPersonID(cursor.getLong(cursor.getColumnIndex(FeedReaderContract.UserContacts.PERSON_ID)));
                userContacts.setUserID(cursor.getLong(cursor.getColumnIndex(FeedReaderContract.UserContacts.USER_ID)));
                userContacts.setGroupID(cursor.getLong(cursor.getColumnIndex(FeedReaderContract.UserContacts.GROUP_ID)));
                userContacts.setPersonContactName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.UserContacts.PERSON_CONTACT_NAME)));
                userContacts.setPersonContactNo(cursor.getString(cursor.getColumnIndex(FeedReaderContract.UserContacts.PERSON_CONTACT_NO)));

                userContactsList.add(userContacts);

                AppLog.i(TAG, "getGroupId --- " + userContacts.getPersonID());
            } while (cursor.moveToNext());
        }

        return userContactsList;
    }

    //----- addUserContact
    public static long addUserContact(long person_id, long user_id, long group_id, String person_contact_name, String person_contact_no) {

        ContentValues values = new ContentValues();

        values.put(FeedReaderContract.UserContacts.PERSON_ID, person_id);
        values.put(FeedReaderContract.UserContacts.USER_ID, user_id);
        values.put(FeedReaderContract.UserContacts.GROUP_ID, group_id);
        values.put(FeedReaderContract.UserContacts.PERSON_CONTACT_NAME, person_contact_name);
        values.put(FeedReaderContract.UserContacts.PERSON_CONTACT_NO, person_contact_no);

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        long addUserId;
        synchronized (database) {
            addUserId = database.insert(FeedReaderContract.UserContacts.TABLE_NAME, null, values);
        }
        return addUserId;
    }

    //----- deleteUserContact
    public static void deleteUserContact(long person_id) {
        if (database == null || !database.isOpen()) {
            getDatabase();
        }

        ContentValues contentValues = new ContentValues();
        synchronized (database) {
            database.delete(FeedReaderContract.UserContacts.TABLE_NAME,
                    FeedReaderContract.UserContacts.PERSON_ID + "=" + person_id, null);
        }
    }

    public static ArrayList<String> getGroupRegisteredUsersNumbers(long group_id) {
        Log.i(TAG, "getAllGroups: ");
        ArrayList<String> contactList = new ArrayList<>();
        String query = "SELECT " + FeedReaderContract.UserContacts.PERSON_CONTACT_NO + " FROM " + FeedReaderContract.UserContacts.TABLE_NAME
                + " WHERE "+ FeedReaderContract.UserContacts.GROUP_ID + "=" + group_id;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        String contactNo;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                contactNo = "+"+cursor.getString(cursor.getColumnIndex(FeedReaderContract.UserContacts.PERSON_CONTACT_NO));

                contactList.add(contactNo);
            } while (cursor.moveToNext());
        }
//        AppLog.i(TAG, "getGroup --- " + group);
        return contactList;
    }

    public static ArrayList<User> getGroupRegisteredUsersInformation(long group_id) {
        ArrayList<User> contactInformation = new ArrayList<>();
        String query = "SELECT *  FROM " + FeedReaderContract.UserContacts.TABLE_NAME
                + " WHERE "+ FeedReaderContract.UserContacts.GROUP_ID + "=" + group_id;
        AppLog.v("UCGC",query);

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        User user = null;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                user = new User();

                user.setUserName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.UserContacts.PERSON_CONTACT_NAME)));
                user.setUserContact("+"+cursor.getString(cursor.getColumnIndex(FeedReaderContract.UserContacts.PERSON_CONTACT_NO)));

                contactInformation.add(user);

            } while (cursor.moveToNext());
        }
//        AppLog.i(TAG, "getGroup --- " + group);
        return contactInformation;
    }


    public static long getGroupMembersCount(long group_id) {
        String query = "SELECT *  FROM " + FeedReaderContract.UserContacts.TABLE_NAME
                + " WHERE "+ FeedReaderContract.UserContacts.GROUP_ID + "=" + group_id;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        long count=0;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                count++;
            } while (cursor.moveToNext());
        }
//        AppLog.i(TAG, "getGroup --- " + group);
        return count;
    }
/*
  ------------------------------------------------------------------------------------
  Operations For Groups
  ------------------------------------------------------------------------------------
*/

    public static void insertAllGroups(ArrayList<Group> groupArrayList){

        for (int i = 0; i < groupArrayList.size(); i++) {

            ContentValues values = new ContentValues();

            values.put(FeedReaderContract.Groups.GROUP_ID, groupArrayList.get(i).getGroupID());
            values.put(FeedReaderContract.Groups.GROUP_NAME, groupArrayList.get(i).getGroupName());
            values.put(FeedReaderContract.Groups.GROUP_DESCRIPTION, groupArrayList.get(i).getGroupDescription());
            values.put(FeedReaderContract.Groups.GROUP_OWNER_ID, groupArrayList.get(i).getGroupOwnerID());

            if (database == null || !database.isOpen()) {
                getDatabase();
            }
            synchronized (database) {
                database.insert(FeedReaderContract.Groups.TABLE_NAME, null, values);
            }
        }
    }


    public static long addGroup(long groupID, String groupName, String groupDescription, long groupOwnerID) {

        ContentValues values = new ContentValues();

        values.put(FeedReaderContract.Groups.GROUP_ID, groupID);
        values.put(FeedReaderContract.Groups.GROUP_NAME, groupName);
        values.put(FeedReaderContract.Groups.GROUP_DESCRIPTION, groupDescription);
        values.put(FeedReaderContract.Groups.GROUP_OWNER_ID, groupOwnerID);

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        long group_id;
        synchronized (database) {
            group_id = database.insert(FeedReaderContract.Groups.TABLE_NAME, null, values);
        }
        return group_id;
    }



    public static ArrayList<Group> getAllGroups() {
        Log.i(TAG, "getAllGroups: ");
        ArrayList<Group> groupList = new ArrayList<>();
        String query = "SELECT * FROM " + FeedReaderContract.Groups.TABLE_NAME;

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor cursor;
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        Group group = null;

        if (cursor != null && cursor.moveToFirst()) {
            do {
                group = new Group();
//
                group.setGroupID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_ID)));
                group.setGroupName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_NAME)));
                group.setGroupDescription(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_DESCRIPTION)));
                group.setGroupOwnerID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_OWNER_ID)));

                groupList.add(group);

                AppLog.i(TAG, "getGroupId --- " + group.getGroupID());
            } while (cursor.moveToNext());
        }

        return groupList;
    }

    public static Group getGroup(long groupId) {

        String query = "SELECT * FROM " +
                FeedReaderContract.Groups.TABLE_NAME +
                " WHERE " + FeedReaderContract.Groups.GROUP_ID + " = " + groupId;

        Cursor cursor = null;
        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        synchronized (database) {
            cursor = database.rawQuery(query, null);
        }

        Group group = null;
        if (cursor != null && cursor.moveToFirst()) {

            group = new Group();

            group.setGroupID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_ID)));
            group.setGroupName(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_NAME)));
            group.setGroupDescription(cursor.getString(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_DESCRIPTION)));
            group.setGroupOwnerID(cursor.getInt(cursor.getColumnIndex(FeedReaderContract.Groups.GROUP_OWNER_ID)));
            AppLog.i(TAG, "getGroupId --- " + group.getGroupID());
        }
        AppLog.i(TAG, "getGroup --- " + group);
        if (cursor != null) {
            cursor.close();
        }
        return group;
    }



}