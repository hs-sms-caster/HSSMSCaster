package com.example.sibtainraza.hssmscaster.database;

/**
 * Created by Sibtain Raza on 12/25/2016.
 */
public class FeedReaderContract {

    public static interface  Users {
        String TABLE_NAME = "Users";

        String USER_ID = "user_id";
        String USER_NAME = "user_name";
        String USER_EMAIL_ID = "user_email_id";
        String USER_CONTACT = "user_contact_no";
    }

    public static interface Groups {

        String TABLE_NAME = "Groups";

        String GROUP_ID = "group_id";
        String GROUP_NAME = "group_name";
        String GROUP_DESCRIPTION = "group_description";
        String GROUP_OWNER_ID = "group_owner_id";
    }

    public static interface UserContacts {
        String TABLE_NAME = "UserContacts";

        String PERSON_ID = "person_id";
        String USER_ID = "user_id" ;
        String GROUP_ID = "group_id";
        String PERSON_CONTACT_NAME = "person_contact_name";
        String PERSON_CONTACT_NO =   "person_contact_no";
    }

    public static interface UserSentMessages {
        String TABLE_NAME = "UserSentMessages";

        String USER_ID = "user_id" ;
        String PERSON_ID = "person_id";
        String GROUP_ID = "group_id";
        String MESSAGE_TEXT = "message_text";
        String TIMESTAMP =   "timestamp";
    }

}
