package com.example.sibtainraza.hssmscaster.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sibtain Raza on 11/5/2016.
 */
public class Group implements Parcelable{

    @SerializedName("group_id")
    @Expose
    private long groupID;

    @SerializedName("group_name")
    @Expose
    private String groupName;

    @SerializedName("group_description")
    @Expose
    private String groupDescription;

    @SerializedName("group_owner_id")
    @Expose
    private long groupOwnerID;

    public Group(){}

    public Group(String groupName, String groupDescription) {
        this.setGroupName(groupName);
        this.setGroupDescription(groupDescription);
    }

    public Group(String groupName, String groupDescription, long groupOwnerID) {
        this.setGroupName(groupName);
        this.setGroupDescription(groupDescription);
        this.setGroupOwnerID(groupOwnerID);
    }

    public Group(long groupID,String groupName, String groupDescription, long groupOwnerID) {
        this.setGroupID(groupID);
        this.setGroupName(groupName);
        this.setGroupDescription(groupDescription);
        this.setGroupOwnerID(groupOwnerID);
    }

    protected Group(Parcel in) {
        groupID = in.readLong();
        groupName = in.readString();
        groupDescription = in.readString();
        groupOwnerID = in.readLong();
    }

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName   ) {
        this.groupName = groupName;
    }


    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public long getGroupOwnerID() {
        return groupOwnerID;
    }

    public void setGroupOwnerID(long groupOwnerID) {
        this.groupOwnerID = groupOwnerID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(groupID);
        parcel.writeString(groupName);
        parcel.writeString(groupDescription);
        parcel.writeLong(groupOwnerID);
    }
}