package com.example.sibtainraza.hssmscaster.model;

/**
 * Created by Sibtain Raza on 12/24/2016.
 */
public class SignIn {

    private String email;
    private String password;

    public SignIn(){}

    public SignIn(String email, String password){
        this.email = email;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
