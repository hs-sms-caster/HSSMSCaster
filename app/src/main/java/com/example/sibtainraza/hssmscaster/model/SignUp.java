package com.example.sibtainraza.hssmscaster.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sibtain Raza on 4/9/2017.
 */

public class SignUp {
    @SerializedName("user_id")
    @Expose
    private long userID;

    @SerializedName("user_name")
    @Expose
    private String userName;

    @SerializedName("user_email_id")
    @Expose
    private String userEmailID;

    @SerializedName("user_contact_no")
    @Expose
    private String userContact;

    private boolean checked;

    public SignUp() {
    }

    public SignUp(long userID, String userName, String userEmailID, String userContact, boolean checked) {
        this.userID = userID;
        this.userName = userName;
        this.userEmailID = userEmailID;
        this.userContact = userContact;
        this.checked = checked;
    }

    public SignUp(String userName, String userEmailID, String userContact, boolean checked) {
        this.userName = userName;
        this.userEmailID = userEmailID;
        this.userContact = userContact;
        this.checked = checked;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getUserContact() {
        return userContact;
    }

    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
