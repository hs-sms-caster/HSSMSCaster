package com.example.sibtainraza.hssmscaster.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sibtain Raza on 11/6/2016.
 */
public class User {
    @SerializedName("user_id")
    @Expose
    private long userID;

    @SerializedName("user_name")
    @Expose
    private String userName;

    @SerializedName("user_contact_no")
    @Expose
    private String userContact;

    private boolean checked;

    public User() {
    }

    public User(String userName, String userContact) {
        this.userName = userName;
        this.userContact = userContact;
    }

    public User(long userID, String userName, String userContact) {
        this.userID = userID;
        this.userName = userName;
        this.userContact = userContact;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getUserContact() {
        return userContact;
    }

    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}