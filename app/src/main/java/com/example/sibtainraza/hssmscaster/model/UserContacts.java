package com.example.sibtainraza.hssmscaster.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sibtain Raza on 4/9/2017.
 */

public class UserContacts {

    @SerializedName("person_id")
    @Expose
    private long personID;

    @SerializedName("user_id")
    @Expose
    private long userID;

    @SerializedName("group_id")
    @Expose
    private long groupID;

    @SerializedName("person_contact_name")
    @Expose
    private String personContactName;

    @SerializedName("person_contact_no")
    @Expose
    private String personContactNo;

    private boolean checked;

    public UserContacts(){

    }

    public UserContacts(long userID, long groupID, long personID) {
        this.userID = userID;
        this.groupID = groupID;
        this.personID = personID;
    }

    public UserContacts(long userID, long groupID, String personContactName, String personContactNo) {
        this.userID = userID;
        this.groupID = groupID;
        this.personContactName = personContactName;
        this.personContactNo = personContactNo;
    }

    public UserContacts(long personID, long userID, long groupID, String personContactName, String personContactNo) {
        this.personID = personID;
        this.userID = userID;
        this.groupID = groupID;
        this.personContactName = personContactName;
        this.personContactNo = personContactNo;
    }



    public long getPersonID() {
        return personID;
    }

    public void setPersonID(long personID) {
        this.personID = personID;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;
    }

    public String getPersonContactName() {
        return personContactName;
    }

    public void setPersonContactName(String personContactName) {
        this.personContactName = personContactName;
    }

    public String getPersonContactNo() {
        return personContactNo;
    }

    public void setPersonContactNo(String personContactNo) {
        this.personContactNo = personContactNo;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}