package com.example.sibtainraza.hssmscaster.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sibtain Raza on 4/9/2017.
 */

public class UserSentMessages {


    @SerializedName("message_id")
    @Expose
    private long messageID;

    @SerializedName("user_id")
    @Expose
    private long userID;

    @SerializedName("group_id")
    @Expose
    private long groupID;

    @SerializedName("message_text")
    @Expose
    private String messageText;

    @SerializedName("message_timestamp")
    @Expose
    private long messageTimestamp;

    public long getMessageID() {
        return messageID;
    }

    public void setMessageID(long messageID) {
        this.messageID = messageID;

    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;

    }

    public long getGroupID() {
        return groupID;
    }

    public void setGroupID(long groupID) {
        this.groupID = groupID;

    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;

    }

    public long getMessageTimestamp() {
        return messageTimestamp;
    }

    public void setMessageTimestamp(long messageTimestamp) {
        this.messageTimestamp = messageTimestamp;

    }

}
