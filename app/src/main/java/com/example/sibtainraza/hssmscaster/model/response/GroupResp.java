package com.example.sibtainraza.hssmscaster.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MuhammadAli on 09-Jan-17.
 */

public class GroupResp {


    @SerializedName("group_id")
    @Expose
    private long groupId;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("is_executed")
    @Expose
    private boolean isExecuted;

    public GroupResp() {
    }

    public GroupResp(long groupId, boolean isExecuted, String message) {
        this.groupId = groupId;
        this.isExecuted = isExecuted;
        this.message = message;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public boolean isExecuted() {
        return isExecuted;
    }

    public void setExecuted(boolean executed) {
        this.isExecuted = executed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
