package com.example.sibtainraza.hssmscaster.model.response;

/**
 * Created by Sibtain Raza on 1/11/2017.
 */

public class ProfileResp {
    private boolean isUpdated;

    public boolean isUpdated() {
        return isUpdated;
    }

    public ProfileResp setUpdated(boolean updated) {
        isUpdated = updated;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ProfileResp setMessage(String message) {
        this.message = message;
        return this;
    }

    private String message;

}
