package com.example.sibtainraza.hssmscaster.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MuhammadAli on 09-Jan-17.
 */

public class UserContactsResp {

    @SerializedName("person_id")
    @Expose
    private long personID;

    @SerializedName("message")
    @Expose
    private String message; //Member Added Or Deleted

    @SerializedName("is_executed")
    @Expose
    private boolean isExecuted;

    public UserContactsResp(){

    }

    public UserContactsResp(long personID, String message, boolean isExecuted) {
        this.personID = personID;
        this.message = message;
        this.isExecuted = isExecuted;
    }

    public long getPersonID() {
        return personID;
    }

    public void setPersonID(long personID) {
        this.personID = personID;
    }

    public boolean isExecuted() {
        return isExecuted;
    }

    public void setExecuted(boolean executed) {
        this.isExecuted = executed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
