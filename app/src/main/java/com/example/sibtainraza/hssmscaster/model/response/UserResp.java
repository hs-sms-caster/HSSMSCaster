package com.example.sibtainraza.hssmscaster.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sibtain Raza on 12/24/2016.
 */
public class UserResp {

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("user_id")
    @Expose
    private long userID;



    @SerializedName("user_email_id")
    @Expose
    private String userEmail;

    @SerializedName("user_name")
    @Expose
    private String userName;

    @SerializedName("is_validate")
    @Expose
    private boolean isValidate;

    @SerializedName("access_token")
    @Expose
    private String accessToken;

    @SerializedName("message")
    @Expose
    private String message;

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;}

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;}

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isValidate() {
        return isValidate;
    }

    public void setValidate(boolean validate) {
        isValidate = validate;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
