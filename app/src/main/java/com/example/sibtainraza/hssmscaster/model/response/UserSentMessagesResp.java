package com.example.sibtainraza.hssmscaster.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MuhammadAli on 09-Jan-17.
 */

public class UserSentMessagesResp {

    @SerializedName("message")
    @Expose
    private String message; //All Message Sent

    @SerializedName("is_executed")
    @Expose
    private boolean isExecuted;

    public UserSentMessagesResp(){
    }

    public UserSentMessagesResp(long personID, String message, boolean isExecuted) {
        this.message = message;
        this.isExecuted = isExecuted;
    }

    public boolean isExecuted() {
        return isExecuted;
    }

    public void setExecuted(boolean executed) {
        this.isExecuted = executed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
