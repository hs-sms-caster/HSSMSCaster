package com.example.sibtainraza.hssmscaster.network;

/**
 * Created by SFSS on 16/01/2017.
 */

public final class NetworkConstants {
    private NetworkConstants(){}

    public static final String BASE = "https://radiant-temple-31017.herokuapp.com";
//    public static final String BASE = "http://192.168.8.101:3000";
    public static final String PROFILE_IMAGES = BASE + "/profileimages/";
    public static final String GROUP_IMAGES = BASE + "/groupimages/";
    public static final String BROADCAST_EVENT = "BROADCAST_EVENT";
    public static final String BROADCAST_EVENT_GROUP_ADD = "BROADCAST_EVENT_GROUP_ADD";
    public static final String BROADCAST_EVENT_NOTIFY_LISTVIEWS = "BROADCAST_EVENT_NOTIFY_LISTVIEWS";


}

