package com.example.sibtainraza.hssmscaster.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.sibtainraza.hssmscaster.HSSMSCaster;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sibtain Raza on 12/24/2016.
 */
public class NetworkHandler {

    private static NetworkHandler instance;
    private Retrofit retrofit;
    private iAppServices services;

    public static NetworkHandler getInstance() {
        return instance == null? (instance = new NetworkHandler()) : instance;
    }

    private NetworkHandler(){
        retrofit = new Retrofit.Builder()
                .baseUrl(NetworkConstants.BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        services = retrofit.create(iAppServices.class);
    }

    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) HSSMSCaster.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net != null && net.isAvailable() && net.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public iAppServices getServices() {
        return services;
    }

}
