package com.example.sibtainraza.hssmscaster.network;

import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.model.SignIn;
import com.example.sibtainraza.hssmscaster.model.SignUp;
import com.example.sibtainraza.hssmscaster.model.User;
import com.example.sibtainraza.hssmscaster.model.UserContacts;
import com.example.sibtainraza.hssmscaster.model.response.GroupResp;
import com.example.sibtainraza.hssmscaster.model.response.UserContactsResp;
import com.example.sibtainraza.hssmscaster.model.response.UserResp;
import com.example.sibtainraza.hssmscaster.model.response.UserSentMessagesResp;

import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface iAppServices {
/*
  ------------------------------------------------------------------------------------
  Api Calls For Users               -       2
  ------------------------------------------------------------------------------------
*/
    @POST("/user/signIn")
    Call<UserResp> loginUser(@Body SignIn signIn);

    @POST("/user/signUp")
    Call<UserResp> addUser(@Body SignUp signUp);

/*
  ------------------------------------------------------------------------------------
  Api Calls For Groups              -       3
  ------------------------------------------------------------------------------------
*/

    @GET("/group/getAllGroups")
    Call<ArrayList<Group>> getAllGroups(@Header("user_id") long user_id);

    @POST("/group/addGroup")
    Call<GroupResp> addGroup(@Body Group group);

    @POST("/group/deleteGroup")
    Call<GroupResp> deleteGroup(@Header("user_id") long user_id);

/*
  ------------------------------------------------------------------------------------
  Api Calls For UserContacts        -       2
  ------------------------------------------------------------------------------------
*/
    @GET("/group/getAllUserContacts")
    Call<ArrayList<UserContacts>> getAllUserContacts(@Header("user_id") long user_id);

    @POST("/userContact/addUserContact")
    Call<UserContactsResp> addUserContact(@Body UserContacts userContacts);

    @POST("/userContact/deleteUserContact")
    Call<UserContactsResp> deleteUserContact(@Body UserContacts userContacts);

/*
  ------------------------------------------------------------------------------------
  Api Calls For UserSentMeesages    -       1
  ------------------------------------------------------------------------------------
*/
//    @POST("/userSentMessage/sendMesssage")
//    Call<UserSentMessagesResp> userSentMessage(long user_id, long group_id, String message_text, ArrayList<User> users);


    @Multipart
    @POST("/userSentMessage/sendMessage")
    Call<UserSentMessagesResp> userSentMessage(@Part("group_id") long user_id,
                                        @Part("user_id") long group_id,
                                        @Part("message_text") String message_text,
                                        @Part("userArrayList") ArrayList<User> userArrayList);
}
