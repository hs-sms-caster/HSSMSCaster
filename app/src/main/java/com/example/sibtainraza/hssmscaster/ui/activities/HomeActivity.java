package com.example.sibtainraza.hssmscaster.ui.activities;

import android.Manifest;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.model.UserContacts;
import com.example.sibtainraza.hssmscaster.model.UserSentMessages;
import com.example.sibtainraza.hssmscaster.network.NetworkHandler;
import com.example.sibtainraza.hssmscaster.ui.fragments.GroupAddFragment;
import com.example.sibtainraza.hssmscaster.ui.fragments.GroupHomeFragment;
import com.example.sibtainraza.hssmscaster.ui.fragments.GroupListFragment;
import com.example.sibtainraza.hssmscaster.ui.fragments.NavigationDrawerFragment;
import com.example.sibtainraza.hssmscaster.utils.AppLog;
import com.example.sibtainraza.hssmscaster.utils.SharedPreferenceManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class HomeActivity extends AppCompatActivity
        implements GroupListFragment.OnGroupListFragmentInteractionListener,
        GroupHomeFragment.OnGroupHomeFragmentInteractionListener,
        GroupAddFragment.OnGroupAddFragmentInteractionListener {

    private static final String TAG = "HomeActivity";
    private FragmentManager fragmentManager;
    private ImageButton btnAllUsers;
    private NavigationDrawerFragment navigationDrawerFragment;
    private DrawerLayout drawerLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            return;
        }

        initViews();
        setListeners();
    }

    private void initViews() {
        if (NetworkHandler.isOnline()) {
            getAllUserGroups();
            Toast.makeText(getApplicationContext(), "Fetching Groups", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        btnAllUsers = (ImageButton) findViewById(R.id.btnAllUsers);

        navigationDrawerFragment = new NavigationDrawerFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frameNavigationDrawer, navigationDrawerFragment)
                .commit();

        fragmentManager = getSupportFragmentManager();
        replaceFragment(new GroupListFragment(), false);

    }

    private void setListeners() {
        btnAllUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "All Employees", Toast.LENGTH_SHORT).show();
                fragmentManager = getSupportFragmentManager();
//                replaceFragment(new AllUsersFragment(), true);
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void replaceFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frameHomeActivity, fragment);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public void getAllUserGroups() {
        Call<ArrayList<Group>> groupList = NetworkHandler.getInstance().getServices()
                .getAllGroups(SharedPreferenceManager.getManager().getUserId());

        groupList.enqueue(new Callback<ArrayList<Group>>() {
            @Override
            public void onResponse(Call<ArrayList<Group>> call, Response<ArrayList<Group>> response) {
                ArrayList<Group> groups = response.body();

                AppLog.v("group", groups + "");
                if (groups != null && groups.size() > 0) {
                    AppLog.d("groupObj", groups.get(0).getGroupID() + "");
                    AppLog.d("groupObj", groups.get(0).getGroupName() + "");
                    AppLog.d("groupObj", groups.get(0).getGroupDescription() + "");
                    AppLog.d("groupObj", groups.get(0).getGroupOwnerID() + "");

                    DbScripts.insertAllGroups(groups);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Group>> call, Throwable t) {

            }
        });
        if (NetworkHandler.isOnline()) {
            getAllUserContacts();
            Toast.makeText(getApplicationContext(), "Fetching UserContacts", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();
        }

    }

    public void getAllUserContacts() {
        Call<ArrayList<UserContacts>> userContactsList = NetworkHandler.getInstance().getServices()
                .getAllUserContacts(SharedPreferenceManager.getManager().getUserId());

        userContactsList.enqueue(new Callback<ArrayList<UserContacts>>() {
            @Override
            public void onResponse(Call<ArrayList<UserContacts>> call, Response<ArrayList<UserContacts>> response) {
                ArrayList<UserContacts> userContacts = response.body();
                AppLog.v("group", userContacts + "");
                if (userContacts != null && userContacts.size() > 0) {
                    AppLog.d("groupObj", userContacts.get(0).getPersonID() + "");
                    AppLog.d("groupObj", userContacts.get(0).getUserID() + "");
                    AppLog.d("groupObj", userContacts.get(0).getGroupID() + "");
                    AppLog.d("groupObj", userContacts.get(0).getPersonContactName() + "");
                    AppLog.d("groupObj", userContacts.get(0).getPersonContactNo() + "");

                    DbScripts.insertAllUserContacts(userContacts);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserContacts>> call, Throwable t) {

            }
        });
    }

    @Override
    public void OnGroupListFragmentInteraction(int constant, @Nullable Object o) {
        switch (constant) {
            case ADD_GROUP:
                replaceFragment(GroupAddFragment.newInstance(), true);
                break;
            case GROUP_HOME:
                replaceFragment(GroupHomeFragment.newInstance((Group) o), true);
                break;
            case GroupListFragment.OnGroupListFragmentInteractionListener.DRAWER_LOCK_CLOSE:
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
            case GroupListFragment.OnGroupListFragmentInteractionListener.DRAWER_LOCK_OPEN:
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
                break;
        }
    }


    @Override
    public void onGroupHomeFragmentInteraction(int constant, @Nullable Object o) {

    }

    @Override
    public void onGroupAddFragmentInteraction(int constant, @Nullable Object o) {
        switch (constant) {
            case GroupAddFragment.OnGroupAddFragmentInteractionListener.BACK:
                fragmentManager.popBackStack();
                break;
        }
    }
}
