package com.example.sibtainraza.hssmscaster.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.SignIn;
import com.example.sibtainraza.hssmscaster.model.response.UserResp;
import com.example.sibtainraza.hssmscaster.network.NetworkHandler;
import com.example.sibtainraza.hssmscaster.utils.AppLog;
import com.example.sibtainraza.hssmscaster.utils.SharedPreferenceManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSION = 1;

    private Button signInButton;
    private EditText emailIdEditText;
    private EditText passwordEditText;

    private String email;
    private String password;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (checkPermission()){
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    Log.i(TAG, "doInBackground: ");
                    DbScripts.getAllGroups();
                    return null;
                }
            }.execute();
        }

        Toast.makeText(getApplicationContext(), "Login Activity (Sign In)",Toast.LENGTH_SHORT).show();
        checkAccessToken();
        initViews();
        setUpListeners();
//        gotoHomeActivity();
    }


    public boolean checkPermission() {
        boolean b = Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (!b) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    }, REQUEST_PERMISSION);
        }

        return b;
    }

    private void initViews() {
        signInButton = (Button) findViewById(R.id.btnSIgnIn);
        emailIdEditText = (EditText) findViewById(R.id.eTEmail);
        passwordEditText = (EditText) findViewById(R.id.eTPassword);

        emailIdEditText.setText("smsrn");
        passwordEditText.setText("smsrn");
    }

    private void setUpListeners() {
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppLog.i(TAG, "onCreate --- signin - onClick");

                if (!validateData()) {
//                            Util.failureToast("Please enter fields");
                    Toast.makeText(LoginActivity.this, "Please enter fields", Toast.LENGTH_SHORT).show();
                    AppLog.i(TAG, "onCreate --- signin - onClick");
//                            return;
                } else {
                    networkCall();
                }
            }
        });

    }

    private boolean validateData(){
        email = emailIdEditText.getText().toString().trim();
        password = passwordEditText.getText().toString().trim();

        if (email.length() == 0 ){
            return false;
        }
        if (password.length() == 0){
            return false;
        }
        return true;
    }

    private void networkCall() {
        if (NetworkHandler.isOnline()) {

            SignIn signIn = new SignIn();

            AppLog.i(TAG, "onCreate --- signin - onClick -- email = " + email +" --- password = " + password);

            signIn.setEmail(email);
            signIn.setPassword(password);

            //USER CLASS HAS TO BE SET AS RESPONSE HERE
            final Call<UserResp> userResp = NetworkHandler.getInstance().getServices().loginUser(signIn);
            userResp.enqueue(new Callback<UserResp>() {
                @Override
                public void onResponse(Call<UserResp> call, Response<UserResp> response) {
                    Toast.makeText(LoginActivity.this, "Response", Toast.LENGTH_SHORT).show();

                    UserResp userResp = response.body();

                    if (userResp.getStatus() == 0 || userResp.getStatus() == 1) {
                        Toast.makeText(LoginActivity.this, "Welcome", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        SharedPreferenceManager.getManager().setUserId(userResp.getUserID());
                        SharedPreferenceManager.getManager().setUserAuth(userResp.getAccessToken());
                        SharedPreferenceManager.getManager().setUserName(userResp.getUserName());
                        SharedPreferenceManager.getManager().setUserEmailId(userResp.getUserEmail());

                        Toast.makeText(LoginActivity.this, "AccessToken:" + userResp.getAccessToken(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(LoginActivity.this, "User ID:" + SharedPreferenceManager.getManager().getUserId(), Toast.LENGTH_SHORT).show();
                        startActivity(intent);
                        finish();
                    }

                    else {
                        Toast.makeText(LoginActivity.this, userResp.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserResp> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Connectivity To Server Error", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Connectivity Error", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkAccessToken() {
        String accessToken = SharedPreferenceManager.getManager().getUserAuth();
        if (accessToken != null && accessToken.length() != 0) {
            gotoHomeActivity();
        }
    }

    private void gotoHomeActivity() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }


}
