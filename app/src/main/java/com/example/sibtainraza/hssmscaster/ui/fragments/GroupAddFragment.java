package com.example.sibtainraza.hssmscaster.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.DeniedByServerException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.model.response.GroupResp;
import com.example.sibtainraza.hssmscaster.network.NetworkConstants;
import com.example.sibtainraza.hssmscaster.network.NetworkHandler;
import com.example.sibtainraza.hssmscaster.utils.AppLog;
import com.example.sibtainraza.hssmscaster.utils.SharedPreferenceManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupAddFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;
    private Context appContext;
    EditText eTGroupName, eTGroupDesc;
    ListView lVGroupMemberAdd;
    Button btnGroupBack, btnAddGroup;
    View rootView;


    private OnGroupAddFragmentInteractionListener mListener;

    public GroupAddFragment() {
    }

    public static GroupAddFragment newInstance() {
        GroupAddFragment fragment = new GroupAddFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        appContext = this.getActivity().getApplicationContext();
        rootView = inflater.inflate(R.layout.fragment_group_add, container, false);

        initViews();
        setListeners();

        return rootView;
    }

    private void initViews() {
        eTGroupName = (EditText) rootView.findViewById(R.id.editTextGroupName);
        eTGroupDesc = (EditText) rootView.findViewById(R.id.editTextGroupDescription);
        lVGroupMemberAdd = (ListView) rootView.findViewById(R.id.listViewGroupMembersAdd);
        btnGroupBack = (Button) rootView.findViewById(R.id.btnGroupBack);
        btnAddGroup = (Button) rootView.findViewById(R.id.btnAddGroup);
    }

    private void sendBroadcast(){
        Intent intent = new Intent(NetworkConstants.BROADCAST_EVENT_GROUP_ADD);
        getContext().sendBroadcast(intent);
    }

    private void setListeners() {
        btnGroupBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    mListener.onGroupAddFragmentInteraction(OnGroupAddFragmentInteractionListener.BACK, null);
            }
        });

        btnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eTGroupName.getText().toString().trim().equals("") || eTGroupDesc.getText().toString().trim().equals("")) {
                    Toast.makeText(getContext(),"Input Fields Required",Toast.LENGTH_SHORT).show();
                }
                else {
                    Group group = new Group(eTGroupName.getText().toString().trim(),
                            eTGroupDesc.getText().toString().trim(),
                            SharedPreferenceManager.getManager().getUserId());
                    final Call<GroupResp> groupRespCall = NetworkHandler.getInstance().getServices().addGroup(group);

                    groupRespCall.enqueue(new Callback<GroupResp>() {
                        @Override
                        public void onResponse(Call<GroupResp> call, Response<GroupResp> response) {
                            GroupResp groupResp = response.body();
                            if (groupResp.isExecuted()) {
                                Toast.makeText(appContext, "Group Added Id:" + groupResp.getGroupId(), Toast.LENGTH_SHORT).show();

                                DbScripts.addGroup(groupResp.getGroupId(),
                                        eTGroupName.getText().toString().trim(),
                                        eTGroupDesc.getText().toString().trim(),
                                        SharedPreferenceManager.getManager().getUserId());
                                mListener.onGroupAddFragmentInteraction(OnGroupAddFragmentInteractionListener.BACK, null);

//                            Toast.makeText(getContext(), "Group Added SQLite", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Group Is Not Added", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GroupResp> call, Throwable t) {
                            Toast.makeText(getContext(), "Group Add - onFailure", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });

        sendBroadcast();


    }

    public void onButtonPressed() {
//        if (mListener != null) {
//            mListener.onGroupAddFragmentInteraction();
//        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnGroupAddFragmentInteractionListener) {
            mListener = (OnGroupAddFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnGroupAddFragmentInteractionListener {
        int BACK = 1;

        void onGroupAddFragmentInteraction(int constant, @Nullable Object o);
    }
}
