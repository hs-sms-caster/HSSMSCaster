package com.example.sibtainraza.hssmscaster.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.adapters.PagerAdapter;
import com.example.sibtainraza.hssmscaster.cores.MenuManagerService;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.utils.AppLog;

public class GroupHomeFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";

    ViewPager mViewPager;
    TabLayout mTabLayout;
    PagerAdapter mPageAdapter;
    private TextView tVGroupName, tVGroupDescription, tvGroupMembersCount;
    private MenuManagerService menuManagerService;
    private View rootView;

    private String mParam1;
    private Group groupObj;

    private OnGroupHomeFragmentInteractionListener mListener;

    public GroupHomeFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static GroupHomeFragment newInstance(Group group) {
        GroupHomeFragment fragment = new GroupHomeFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1,group);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupObj = getArguments().getParcelable(ARG_PARAM1);
            AppLog.d("GroupObject - ",groupObj.getGroupName()+"");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_home, container, false);
        menuManagerService = new MenuManagerService();

        initViews();
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        return rootView;
    }

    private void initViews() {
        tVGroupName =(TextView) rootView.findViewById(R.id.tvGroupName);
        tVGroupDescription = (TextView) rootView.findViewById(R.id.tvGroupDescription);
        tvGroupMembersCount = (TextView) rootView.findViewById(R.id.tvGroupMembersCount);

        tVGroupName.setText(groupObj.getGroupName().toString().trim());
        tVGroupDescription.setText(groupObj.getGroupDescription().toString().trim());
        tvGroupMembersCount.setText(DbScripts.getGroupMembersCount(groupObj.getGroupID())+"");

        mTabLayout = (TabLayout) rootView.findViewById(R.id.homeActivityTabLayout);
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(R.layout.grouptablayouttab1view));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(R.layout.grouptablayouttab2view));
        mTabLayout.addTab(mTabLayout.newTab().setCustomView(R.layout.grouptablayouttab3view));

        mViewPager = (ViewPager) rootView.findViewById(R.id.homeActivityViewPager);
        mPageAdapter = new com.example.sibtainraza.hssmscaster.adapters.PagerAdapter(getChildFragmentManager(),mTabLayout.getTabCount(),groupObj);

        mViewPager.setAdapter(mPageAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void onButtonPressed() {
//        if (mListener != null) {
//            mListener.onGroupHomeFragmentInteraction();
//        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupHomeFragmentInteractionListener) {
            mListener = (OnGroupHomeFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnGroupHomeFragmentInteractionListener {
        int GROUP_SETTING = 2;

        void onGroupHomeFragmentInteraction(int constant, @Nullable Object o);
    }
}
