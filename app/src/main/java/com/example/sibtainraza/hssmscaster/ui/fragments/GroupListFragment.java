package com.example.sibtainraza.hssmscaster.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.adapters.HomeFragmentCardAdapter;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.Group;

import java.util.ArrayList;

public class GroupListFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;

    ImageButton btnAddGroup;
    RecyclerView recyclerView;
    HomeFragmentCardAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Group> groupCard = new ArrayList<Group>();
    private View rootView;

    String name;

    BroadcastReceiver broadcastCastEventGroupAdd;



    private OnGroupListFragmentInteractionListener mListener;

    public GroupListFragment() {
    }

    public static GroupListFragment newInstance(String param1, String param2) {
        GroupListFragment fragment = new GroupListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_list, container, false);

        broadcastCastEventGroupAdd = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                long a =intent.getIntExtra("GROUPID",-1);
                updateGroup();
            }
        };

        initViews();
        setListeners();
        setUpAdapter();

        return rootView;
    }

    private void updateGroup() {

    }

    private void initViews() {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        btnAddGroup = (ImageButton) rootView.findViewById(R.id.btnAddGroup);
    }

    private void setListeners() {
        btnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.OnGroupListFragmentInteraction(OnGroupListFragmentInteractionListener.ADD_GROUP, null);
            }
        });
    }

    int count = 0;

    private void setUpAdapter() {

//        for (int i = 1; i < 16; i++) {
//            Group group = new Group("GroupName : " + i, "Group Description : " + i);
//            count++;
//            groupCard.add(group);
//        }

        try {
            groupCard = DbScripts.getAllGroups();
        }catch (Exception e){
            e.printStackTrace();
        }

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setHasFixedSize(true);
        adapter = new HomeFragmentCardAdapter(groupCard);
        adapter.setItemSelectedListener(new HomeFragmentCardAdapter.ItemSelectedListener() {
            @Override
            public void onItemSelected(Group group) {
                mListener.OnGroupListFragmentInteraction(OnGroupListFragmentInteractionListener.GROUP_HOME, group);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    public void onButtonPressed() {
//        if (mListener != null) {
//            mListener.OnGroupListFragmentInteraction(uri);
//        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGroupListFragmentInteractionListener) {
            mListener = (OnGroupListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGroupListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

//        getContext().unregisterReceiver(broadcastCastEventGroupAdd);
    }

    public interface OnGroupListFragmentInteractionListener {
        int ADD_GROUP = 1;
        int GROUP_HOME = 2;
        int DRAWER_LOCK_CLOSE = 4;
        int DRAWER_LOCK_OPEN = 5;

        void OnGroupListFragmentInteraction(int constant, @Nullable Object o);
    }
}
