package com.example.sibtainraza.hssmscaster.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.adapters.UserAddDeleteMemberAdapter;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.model.User;
import com.example.sibtainraza.hssmscaster.network.NetworkConstants;
import com.example.sibtainraza.hssmscaster.utils.AppLog;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GroupTabLayoutFragment1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GroupTabLayoutFragment1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupTabLayoutFragment1 extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;
    private Group groupObj;

    View rootView;
    ListView lVAddGroupMembers;
    ArrayList<User> listUserAddMembers = new ArrayList<>();

    private OnFragmentInteractionListener mListener;

    BroadcastReceiver broadcastReceiver;

    public GroupTabLayoutFragment1() {
    }

    public static GroupTabLayoutFragment1 newInstance(Group group) {
        GroupTabLayoutFragment1 fragment = new GroupTabLayoutFragment1();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1,group);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupObj =  getArguments().getParcelable(ARG_PARAM1);
            AppLog.d("GroupTabLayoutFragment1 - ", groupObj.getGroupName()+"");
            Toast.makeText(getContext(),"GroupTabLayoutFragment1"+groupObj.getGroupName(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_tab_layout_fragment1, container, false);
        lVAddGroupMembers = (ListView) rootView.findViewById(R.id.listViewAddGroupMembers);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateList();
            }
        };

        IntentFilter intentFilter = new IntentFilter(NetworkConstants.BROADCAST_EVENT_NOTIFY_LISTVIEWS);
        getContext().registerReceiver(broadcastReceiver, intentFilter);
        return rootView;
    }

    private void updateList() {
        setAdapter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setAdapter();
    }

    private void setAdapter() {
        listUserAddMembers = DbScripts.getGroupRegisteredUsersInformation(groupObj.getGroupID());
        UserAddDeleteMemberAdapter userAddMemberAdapter = new UserAddDeleteMemberAdapter(getContext(), UserAddDeleteMemberAdapter.GROUP_MEMBER_DELETE, listUserAddMembers,groupObj);
        lVAddGroupMembers.setAdapter(userAddMemberAdapter);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnGroupListFragmentInteractionListener) {
//            mListener = (OnGroupListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnGroupListFragmentInteractionListener");
//        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getContext().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

}
