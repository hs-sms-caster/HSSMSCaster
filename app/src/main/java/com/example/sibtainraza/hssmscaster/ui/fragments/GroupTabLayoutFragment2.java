package com.example.sibtainraza.hssmscaster.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.adapters.UserMemberSelectionAdapter;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.model.User;
import com.example.sibtainraza.hssmscaster.model.UserContacts;
import com.example.sibtainraza.hssmscaster.model.response.UserSentMessagesResp;
import com.example.sibtainraza.hssmscaster.network.NetworkConstants;
import com.example.sibtainraza.hssmscaster.network.NetworkHandler;
import com.example.sibtainraza.hssmscaster.utils.AppLog;
import com.example.sibtainraza.hssmscaster.utils.SharedPreferenceManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupTabLayoutFragment2 extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;
    private Group groupObj;

    View rootView;
    CheckBox cBSelectAll;
    ListView lVGroupMembersSelection;
    ArrayList<User> listUserAddMembers = new ArrayList<>();
    EditText eTMessage;
    Button btnSendMessage;
    UserMemberSelectionAdapter userMemberSelectionAdapter;

    BroadcastReceiver broadcastReceiver;

    private OnFragmentInteractionListener mListener;

    public GroupTabLayoutFragment2() {
    }

    public static GroupTabLayoutFragment2 newInstance(Group group) {
        GroupTabLayoutFragment2 fragment = new GroupTabLayoutFragment2();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1,group);
        //args.putString(ARG_PARAM1, param1); //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupObj =  getArguments().getParcelable(ARG_PARAM1);
            AppLog.d("GroupTabLayoutFragment2 - ", groupObj.getGroupName()+"");
            Toast.makeText(getContext(),"GroupTabLayoutFragment2"+groupObj.getGroupName(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_tab_layout_fragment2, container, false);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateList();
            }
        };

        IntentFilter intentFilter = new IntentFilter(NetworkConstants.BROADCAST_EVENT_NOTIFY_LISTVIEWS);
        getContext().registerReceiver(broadcastReceiver, intentFilter);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cBSelectAll = (CheckBox) rootView.findViewById(R.id.checkBoxSelectAll);
        lVGroupMembersSelection = (ListView) rootView.findViewById(R.id.listViewGroupMembersSelect);
        eTMessage = (EditText) rootView.findViewById(R.id.eTMessage);
        btnSendMessage = (Button) rootView.findViewById(R.id.btnMessageSend);

        setAdapter();

        cBSelectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                userMemberSelectionAdapter.selectAll(isChecked);
            }
        });

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Message Send",Toast.LENGTH_SHORT).show();

                Call<UserSentMessagesResp> userSentMessagesRespCall =
                        NetworkHandler.getInstance().getServices().userSentMessage (SharedPreferenceManager.getManager().getUserId(),
                                                                                    groupObj.getGroupID(),
                                                                                    eTMessage.getText().toString().trim(),
                                                                                    userMemberSelectionAdapter.getSelectedUser());

                userSentMessagesRespCall.enqueue(new Callback<UserSentMessagesResp>() {
                    @Override
                    public void onResponse(Call<UserSentMessagesResp> call, Response<UserSentMessagesResp> response) {
                        Toast.makeText(getContext(), "Completed",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<UserSentMessagesResp> call, Throwable t) {

                    }
                });
            }
        });
    }

    private void setAdapter() {
        listUserAddMembers = DbScripts.getGroupRegisteredUsersInformation(groupObj.getGroupID());
//        ArrayList<User> listUserMembers =
//        Log.v("FINAL CONTACTS", String.valueOf(listUserAddMembers.get(0).getUserName()));
        userMemberSelectionAdapter = new UserMemberSelectionAdapter(getContext(),listUserAddMembers);
        lVGroupMembersSelection.setAdapter(userMemberSelectionAdapter);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnGroupListFragmentInteractionListener) {
//            mListener = (OnGroupListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnGroupListFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getContext().unregisterReceiver(broadcastReceiver);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void updateList(){
        setAdapter();
    }

}
