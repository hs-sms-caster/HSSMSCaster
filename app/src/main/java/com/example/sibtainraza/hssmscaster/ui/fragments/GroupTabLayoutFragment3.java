package com.example.sibtainraza.hssmscaster.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;
import com.example.sibtainraza.hssmscaster.adapters.UserAddDeleteMemberAdapter;
import com.example.sibtainraza.hssmscaster.database.DbScripts;
import com.example.sibtainraza.hssmscaster.model.Group;
import com.example.sibtainraza.hssmscaster.model.User;
import com.example.sibtainraza.hssmscaster.model.UserContacts;
import com.example.sibtainraza.hssmscaster.model.response.UserContactsResp;
import com.example.sibtainraza.hssmscaster.network.NetworkConstants;
import com.example.sibtainraza.hssmscaster.network.NetworkHandler;
import com.example.sibtainraza.hssmscaster.utils.AppLog;
import com.example.sibtainraza.hssmscaster.utils.SharedPreferenceManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupTabLayoutFragment3 extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;
    private Group groupObj;

    View rootView;
    ListView lVAddGroupMembers;
    ArrayList<User> listUserAddMembers ;
    UserAddDeleteMemberAdapter userAddMemberAdapter;


    private OnFragmentInteractionListener mListener;

    public GroupTabLayoutFragment3() {
    }

    public static GroupTabLayoutFragment3 newInstance(Group group) {
        GroupTabLayoutFragment3 fragment = new GroupTabLayoutFragment3();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1,group);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupObj =  getArguments().getParcelable(ARG_PARAM1);
            AppLog.d("GroupTabLayoutFragment3 - ", groupObj.getGroupName()+"");
            Toast.makeText(getContext(),"GroupTabLayoutFragment3"+groupObj.getGroupName(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_group_tab_layout_fragment3, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lVAddGroupMembers = (ListView) rootView.findViewById(R.id.listViewAddGroupMembers);



        listUserAddMembers = new ArrayList<User>(readContacts().values());
        Collections.sort(listUserAddMembers, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getUserName().compareTo(o2.getUserName());
            }
        });
        for (User user : listUserAddMembers) {
            System.out.println(user.getUserName() + " " + user.getUserContact());
        }

//        Log.v("FINAL CONTACTS", String.valueOf(listUserAddMembers.get(0).getUserName()));

        setAdapter();

    }

    private void setAdapter() {
        userAddMemberAdapter = new UserAddDeleteMemberAdapter(getContext(), UserAddDeleteMemberAdapter.GROUP_MEMBER_ADD, listUserAddMembers,groupObj);
        lVAddGroupMembers.setAdapter(userAddMemberAdapter);
        userAddMemberAdapter.setClickEventCallback(new UserAddDeleteMemberAdapter.ClickEventCallback() {
            @Override
            public void onItemSelected(final User user) {

                NetworkCall(user);

            }
        });
    }

    private void NetworkCall(final User user) {
        UserContacts AddUserContact = new UserContacts(SharedPreferenceManager.getManager().getUserId(),
                groupObj.getGroupID(),
                user.getUserName(),
                user.getUserContact());

        Call<UserContactsResp> addUserContactsRespCall = NetworkHandler.getInstance().getServices().addUserContact(AddUserContact);

        addUserContactsRespCall.enqueue(new Callback<UserContactsResp>() {
            @Override
            public void onResponse(Call<UserContactsResp> call, Response<UserContactsResp> response) {
                UserContactsResp userContactsResp = response.body();
                if (userContactsResp.isExecuted()) {
                    Toast.makeText(getContext(), "UserContact Added Id:" + userContactsResp.getPersonID(), Toast.LENGTH_SHORT).show();

                    addIndatabase(userContactsResp, user);
//                            Toast.makeText(getContext(), "Group Added SQLite", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "UserContact Is Not Added", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserContactsResp> call, Throwable t) {
//                Toast.makeText(getContext(), "UserContact - onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addIndatabase(final UserContactsResp userContactsResp, final User user) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                DbScripts.addUserContact(userContactsResp.getPersonID(),
                        SharedPreferenceManager.getManager().getUserId(),
                        groupObj.getGroupID(),
                        user.getUserName(),
                        user.getUserContact());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                userAddMemberAdapter.removeItem(user);
                sendBroadcast();
            }
        }.execute();
    }

    private void sendBroadcast() {
        Intent intent = new Intent(NetworkConstants.BROADCAST_EVENT_NOTIFY_LISTVIEWS);
        getContext().sendBroadcast(intent);
    }


    public void onButtonPressed() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }

    private HashMap<String, User> readContacts() {
        int i = 0;

        ArrayList<String> registeredUsers;
        HashMap<String, User> map = new HashMap<>();
        AppLog.v("GroupIDCONTACT",groupObj.getGroupID()+"");
        registeredUsers = DbScripts.getGroupRegisteredUsersNumbers(groupObj.getGroupID());

        Cursor phones = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

        if (phones != null && phones.moveToFirst()) {
            do {

                String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = (phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))).replace(" ","");

                if(phoneNumber.length()==0 ||
                        phoneNumber.contains("#") ||
                        phoneNumber.contains("*") ||
                        phoneNumber.length()<11 ||
                        phoneNumber.length()>13 ||
                        phoneNumber==null) {
                    continue;
                }

                if(phoneNumber.length()==11 && phoneNumber.charAt(0)=='0'){
                    phoneNumber = "+92" + phoneNumber.substring(1);
                }

                if(!phoneNumber.contains("+92")){
                    continue;
                }

                if(registeredUsers.contains(phoneNumber)){
                    continue;
                }

                User user = new User();
                user.setUserName(name);
                user.setUserContact(phoneNumber);

                map.put(phoneNumber,user);
                i++;

            } while (phones.moveToNext());
        } else if (phones == null) {
            AppLog.d("RD1", "null phones");
        } else {
            AppLog.d("RD2", phones.getCount() + "");
        }

        if(phones != null) {
            AppLog.d("RD3", phones.getCount() + " --- " + i);
            phones.close();
        }
        return map;

    }
}