package com.example.sibtainraza.hssmscaster.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sibtainraza.hssmscaster.R;


public class NavigationDrawerFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    private ImageView userProfileImage;
    private TextView userName, userEmail;
    private ListView userCheckInGroupsLV;
    private Button checkInOut, logOut;
    TextView statusTxt;

    private ScrollView navigationDrawerLayout;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    private View rootView;
    private OnNavigationDrawerFragmentInteractionListener mListener;

    public NavigationDrawerFragment() {
    }

    public static NavigationDrawerFragment newInstance(String param1, String param2) {
        NavigationDrawerFragment fragment = new NavigationDrawerFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

//        boolean s = SharedPreferenceManager.getManager().getCheckedInStatus();
//        int checkedInFenceId = SharedPreferenceManager.getManager().getCheckedInFenceId();
//        pref = getContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//        editor = pref.edit();
        Toast.makeText(getContext(), "User Profile, Fences and Logout",Toast.LENGTH_SHORT).show();
        initViews();
//        setViews();
        setListeners();
        setUpAdapter();

        return rootView;
    }


    private void initViews() {
        userProfileImage = (ImageView) rootView.findViewById(R.id.navigation_UserProfileImage);
        userName = (TextView) rootView.findViewById(R.id.navigation_TW_UserName);
        userEmail = (TextView) rootView.findViewById(R.id.navigation_TW_UserEmail);
        userCheckInGroupsLV = (ListView) rootView.findViewById(R.id.navigation_ListView);
        logOut = (Button) rootView.findViewById(R.id.navigation_BtnLogOut);
        statusTxt = (TextView) rootView.findViewById(R.id.statusTxt);
        navigationDrawerLayout = (ScrollView) rootView.findViewById(R.id.navigationDrawerLayout);
    }

    private void setViews() {

    }

    private void setListeners() {
        userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Profile Image Clicked", Toast.LENGTH_SHORT).show();
                mListener.OnNavigationDrawerFragmentInteraction(OnNavigationDrawerFragmentInteractionListener.SHOW_PROFILE,null);
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Check InOut Clicked", Toast.LENGTH_SHORT).show();
                mListener.OnNavigationDrawerFragmentInteraction(OnNavigationDrawerFragmentInteractionListener.LOGOUT,null);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnNavigationDrawerFragmentInteractionListener) {
//            mListener = (OnNavigationDrawerFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnNavigationDrawerFragmentInteractionListener");
//        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnNavigationDrawerFragmentInteractionListener {
        int LOGOUT = 1;
        int SHOW_PROFILE = 2;
        void OnNavigationDrawerFragmentInteraction(int constant, @Nullable Object o);
    }

    public void setUpAdapter() {
        /*listNavDrawer = new ArrayList<>();
        try {
            listNavDrawer = DbScripts.getAllFenceGroups();
        }catch (Exception e){
            e.printStackTrace();
        }
        userCheckInGroupsLV.setAdapter(new NavigationDrawerListAdapter(getContext(), listNavDrawer));*/
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean perm = false;
        for (int i = 0; i < permissions.length; i++) {

            String s = permissions[i];
            int j = grantResults[i];
            if (s.equals(Manifest.permission.RECORD_AUDIO)) {

                perm = j == PackageManager.PERMISSION_GRANTED;

            }
        }

        if (requestCode == 0 && perm) {

            Toast.makeText(getContext(), "Permission granted", Toast.LENGTH_SHORT).show();
            setUpAdapter();

        }

    }
}
