package com.example.sibtainraza.hssmscaster.utils;

import android.util.Log;

/**
 * Created by zeeshan on 5/1/2015.
 */
public class AppLog {

    private static final String CONSTANT_TAG = "APP:";

    public static int v(String tag, String msg) {
        return Log.v(tag,msg);
    }

    public static int v(String tag, String msg, Throwable tr) {
        return Log.v(CONSTANT_TAG + tag,msg,tr);
    }

    public static int d(String tag, String msg) {
        return Log.d(CONSTANT_TAG + tag,msg);
    }


    public static int d(String tag, String msg, Throwable tr) {
        return Log.d(CONSTANT_TAG + tag,msg,tr);
    }

    public static int i(String tag, String msg) {
        return Log.i(CONSTANT_TAG + tag,msg);
    }

    public static int i(String tag, String msg, Throwable tr) {
        return Log.i(CONSTANT_TAG + tag,msg,tr);
    }

    public static int w(String tag, String msg) {
        return Log.w(CONSTANT_TAG + tag,msg);
    }

    public static int w(String tag, String msg, Throwable tr) {
        return Log.w(CONSTANT_TAG + tag,msg,tr);
    }

    public static int w(String tag, Throwable tr) {
        return Log.w(CONSTANT_TAG + tag,tr);
    }

    public static int e(String tag, String msg) {
        return Log.e(CONSTANT_TAG + tag,msg);
    }

    public static int e(String tag, String msg, Throwable tr) {
        return Log.e(CONSTANT_TAG + tag,msg,tr);
    }

}
