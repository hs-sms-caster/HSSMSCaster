package com.example.sibtainraza.hssmscaster.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.example.sibtainraza.hssmscaster.HSSMSCaster;

/**
 * Created by Sibtain Raza on 12/24/2016.
 */

public class SharedPreferenceManager {


    public static final String TAG = SharedPreferenceManager.class.getSimpleName();
    private SharedPreferences preferences;

    static {
        getManager();
    }

    private static SharedPreferenceManager manager;

    public void clear(){
        SharedPreferenceManager.getManager().preferences.edit().clear().commit();
    }

    public static SharedPreferenceManager getManager() {
        if (manager == null) {
            manager = new SharedPreferenceManager();
        }
        return manager;
    }

    public SharedPreferenceManager() {
        preferences = HSSMSCaster.getInstance().getSharedPreferences("APP", Context.MODE_PRIVATE);
    }

    //--------------------USER_AUTH--------------------//
    public String getUserAuth() {
        String auth = preferences.getString(Constants.USER_AUTH, null);
        AppLog.i(TAG,"getAuth --- " + auth);
        return auth;
    }
    public void setUserAuth(String auth) {
        preferences.edit().putString(Constants.USER_AUTH, auth).apply();
    }

    //--------------------USER_ID--------------------//
    public long getUserId() {
        long userId = preferences.getLong(Constants.USER_ID, -1);
        AppLog.i(TAG,"getUserId --- " + userId);
        return userId;
    }
    public void setUserId(long userId) {
        preferences.edit().putLong(Constants.USER_ID, userId).apply();
    }

    //--------------------USER_EMAIL--------------------//
    public String getUserEmailId() {
        String userEmailId = preferences.getString(Constants.USER_EMAIL, null);
        AppLog.i(TAG,"getuserEmailId --- " + userEmailId);
        return userEmailId;
    }
    public void setUserEmailId(String userEmailId) {
        preferences.edit().putString(Constants.USER_EMAIL, userEmailId).apply();
    }

    //--------------------USER_FIRST_NAME--------------------//
    public String getUserName() {
        String userName = preferences.getString(Constants.USER_NAME, null);
        AppLog.i(TAG,"getuserName --- " + userName);
        return userName;
    }
    public void setUserName(String userName) {
        preferences.edit().putString(Constants.USER_NAME, userName).apply();
    }

    //--------------------MESSAGE_REQUEST_TIME--------------------//
    public long getMessageRequestTime() {
        long messageRequestTime = preferences.getLong(Constants.MESSAGE_REQUEST_TIME, -1);
        AppLog.i(TAG,"messageRequestTime --- " + messageRequestTime);
        return messageRequestTime;
    }
    public void setMessageRequestTime(long messageRequestTime) {
        preferences.edit().putLong(Constants.MESSAGE_REQUEST_TIME, messageRequestTime).apply();
    }


    interface Constants {
        String USER_AUTH = "USER_AUTH";

        String USER_ID = "USER_ID";
        String USER_EMAIL = "USER_EMAIL";
        String USER_NAME = "USER_FIRST_NAME";
        String MESSAGE_REQUEST_TIME = "MESSAGE_REQUEST_TIME";
    }

}
